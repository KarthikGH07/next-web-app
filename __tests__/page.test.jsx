import "@testing-library/jest-dom";
import { render, screen } from "@testing-library/react";
import Page from "@/app/page";

describe("Page", () => {
  it("renders 5 links", () => {
    render(<Page />);

    const links = screen.getAllByRole("link");

    expect(links).toHaveLength(5);
  });
});
